import React from 'react';
import Link from 'next/link';

type Props = {

};

const Sidebar: React.FC<Props> = ({ children }) => {
  return (
    <>
      <div>Sidebar</div>
      <div className='w-full max-w-sm'>
        <Link href="/">
          <a>
            <div className='p-4 mb-4 border w-full'>Index</div>
          </a>
        </Link>
        <Link href="/dashboard">
          <a>
            <div className='p-4 mb-4 border w-full'>Dashboard</div>
          </a>
        </Link>
        <Link href="/server-side">
          <a>
            <div className='p-4 mb-4 border w-full'>Server Side</div>
          </a>
        </Link>
      </div>
    </>
  );
};

export default Sidebar;