import React, { useContext, useEffect } from 'react';
import { signIn, signOut, useSession } from 'next-auth/react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import UserContext from '@stores/userProvider';


type Props = {

};

const Header: React.FC<Props> = () => {
  const { data: session, status } = useSession();
  const loading = status === 'loading';
  const router = useRouter();
  const { user, setUser } = useContext(UserContext);


  useEffect(() => {
    if (!loading && session === null) {
      router.push('/sign-in');
    }
  }, [loading, session]);

  useEffect(() => {
    if (session) {
      // call api init user get res user
      // success set context 
      // error call logout
      const res = true;
      if (res) {
        const logginUser = {
          email: 'jihanlugas2@gmail.com',
          username: 'jihanlugas',
          fullname: 'Jihan Lugas',
          userId: 0,
        };
        setUser(logginUser);
      } else {
        handleSignOut();
      }
    }
  }, [session]);

  const handleSignOut = () => {
    // call api logout
    setUser(null);
    signOut({ callbackUrl: '/sign-in', redirect: true });
  };

  return (
    <>
      <div>
        <div className='px-4 h-16 shadow'>
          <div className='flex justify-between items-center h-16'>
            <Link href={'/'}>
              <a>
                Go To Home
              </a>
            </Link>
            {user && (
              <div>
                <span className={'mr-4'}>
                  <strong>{user.fullname}</strong>
                </span>
                <button
                  className={''}
                  onClick={handleSignOut}
                >
                  Sign out
                </button>
              </div>
            )}
          </div>
        </div>
      </div >
    </>
  );
};

export default Header;