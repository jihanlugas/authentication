import React, { useContext } from 'react';
import { signIn, signOut, useSession } from 'next-auth/react';
import UserContext from '@stores/userProvider';

type Props = {

};

const Header: React.FC<Props> = () => {
  const { data: session, status } = useSession();
  const { user, setUser } = useContext(UserContext);

  return (
    <>
        <div className='p-4'>
          <div className='whitespace-pre-wrap'>{JSON.stringify(session, null, 4)}</div>
          <div className='whitespace-pre-wrap'>{JSON.stringify(status, null, 4)}</div>
          <div className='whitespace-pre-wrap'>{JSON.stringify(user, null, 4)}</div>
      </div >
    </>
  );
};

export default Header;