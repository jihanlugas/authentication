import React from 'react';
import Header from '@com/Layout/Header';
import Footer from '@com/Layout/Footer';
import Sidebar from '@com/Layout/Sidebar';

type Props = {

};

const Main: React.FC<Props> = ({ children }) => {
  return (
    <>
      <main className={''}>
        <Header />
        <Sidebar />
        {children}
        <Footer />
      </main>
    </>
  );
};

export default Main;