import { ClientSafeProvider, getProviders, signIn, useSession } from 'next-auth/react';
import { GetServerSideProps } from 'next/types';
import Link from 'next/link';
import PageWithLayoutType from '@type/layout';
import Main from '@com/Layout/Main';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import Footer from '@com/Layout/Footer';

type Props = {
  providers: ClientSafeProvider
}

const SignIn: React.FC<Props> = ({ providers }) => {
  const { data: session, status } = useSession();
  const loading = status === 'loading';
  const router = useRouter();
  
  return (
    <>
      <div>
        <div className='w-full flex justify-center'>
          <div className='w-full max-w-sm'>
            <div>
              <div className='mb-8'>Sign In Page</div>
              {Object.values(providers).map((provider) => (
                <div key={provider.name} className={'mb-8'}>
                  <button onClick={() => signIn(provider.id, { callbackUrl: '/', redirect: true })} className={'bg-purple-300 w-full p-4 rounded-md'}>
                    Sign in with {provider.name}
                  </button>
                </div>
              ))}
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};

// (SignIn as PageWithLayoutType).layout = Main;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const providers = await getProviders();
  return {
    props: { providers },
  };
};


export default SignIn;