import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/Home.module.css';

type Props = {

}

const Dashboard: React.FC<Props> = () => {
  return (
    <>
      <div>Dashboard</div>
    </>
  );
};

(Dashboard as PageWithLayoutType).layout = Main;


export default Dashboard;
