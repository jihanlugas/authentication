import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';

type Props = {

}

const Index: React.FC<Props> = () => {
  return (
    <>
      <div>
        <div>Index</div>
      </div>
    </>
  );
};

(Index as PageWithLayoutType).layout = Main;


export default Index;
