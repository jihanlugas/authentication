import '../styles/globals.css';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import { SessionProvider } from 'next-auth/react';
import PageWithLayoutType from '@type/layout';
import { UserContextProvider } from '@stores/userProvider';



type AppLayoutProps = {
  Component: PageWithLayoutType
  pageProps: any
}

const MyApp: React.FC<AppLayoutProps> = ({ Component, pageProps }) => {
  const Layout = Component.layout || (({ children }) => <>{children}</>);

  return (
    <>
      <Head>
        <title>Authentication</title>
      </Head>
      <SessionProvider
        session={pageProps.session}
        refetchInterval={0}
      >
        <UserContextProvider>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </UserContextProvider>
      </SessionProvider>
    </>
  );
};

export default MyApp;
