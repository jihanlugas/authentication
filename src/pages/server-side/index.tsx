import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import type { GetServerSideProps, NextPage } from 'next';
import { Session } from 'next-auth';
import { getSession, SessionContextValue } from 'next-auth/react';
import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/Home.module.css';

type Props = {
  session: Session
}

const ServerSide: React.FC<Props> = ({ session }) => {
  
  return (
    <>
      <div>Server Side</div>
    </>
  );
};

(ServerSide as PageWithLayoutType).layout = Main;


export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);
  return {
    props: {
      session: session,
    },
  };
};


export default ServerSide;
